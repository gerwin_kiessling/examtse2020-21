## Student Project: Programming in Python

In this project we simulate a coffee bar. The instructions can be found in the powerpoint presentation inside the Documentation folder.
The code is written by Gerwin Kiessling and Felix Kirsch. The project is part of the Programming in Python lecture at the Toulouse School of Economics.