import datetime
import numpy as np
import pandas as pd

from .coffee_bar_classes import Coffee_Bar, Returning_Customer, One_Time_Customer, \
                                Tripadvisor_Customer, Hipster



def prepare_simulation(drink_prices, food_prices, budget_returner = 250, no_ret_customers = 667, no_hipster = 333):
    """
    Prepare Simulation as defined in Documentation/ExamProjectTSE_2020-21.pptx

    :return: Coffee_Bar instance, list of returning customers
    """

    coffee_bar = Coffee_Bar(drink_prices, food_prices)

    reg_ret_customers = list()
    for i in range(0, no_ret_customers):
        reg_ret_customers.append(Returning_Customer(coffee_bar, budget_returner))

    hipster = list()
    for i in range(0, no_hipster):
        hipster.append(Hipster(coffee_bar))

    return coffee_bar, reg_ret_customers + hipster


def simulate(coffee_bar, ret_customers, start_date = datetime.date(2016, 1, 1), \
             end_date = datetime.date(2020, 12, 31)):
    """
    Simulates activities in the coffee bar.

    :param coffee_bar: Coffee_bar instance
    :param ret_customers: list of Returning Customers, entries are removed inside the method
    :param start_date: simulation start, used for iterating
    :param end_date: simulation end
    """

    ret_customers = ret_customers[:]
    timeslots = get_timeslots()
    one_day = datetime.timedelta(days=1)

    # run the simulation in the given time
    while start_date <= end_date:

        for timeslot in timeslots:

            # create a datetime object for the timeslot
            time = datetime.datetime.combine(start_date, timeslot)

            # random choice of the customer type
            # returning customers are chosen only if there are ones with enough budget
            # after that only one time customers can be chosen
            if ret_customers:
                customer_type = np.random.choice(['returning', 'one_time', 'tripadvisor'], 1, p=[0.2, 0.72, 0.08])[0]
            else:
                customer_type = np.random.choice(['one_time', 'tripadvisor'], 1, p=[0.9, 0.1])[0]

            if customer_type == 'returning':
                # choose a random returning customer and call the buy method
                ret_customer = np.random.choice(ret_customers, 1)[0]
                ret_customer.buy(time)

                # check if he has enough money and remove him from the list if not
                if not ret_customer.is_able_to_buy_most_expensive_food_drink():
                    ret_customers.remove(ret_customer)

            elif customer_type == 'one_time':
                one_time_customer = One_Time_Customer(coffee_bar)
                one_time_customer.buy(time)
            else:
                tripadvisor_customer = Tripadvisor_Customer(coffee_bar)
                tripadvisor_customer.buy(time)

        # print the current progress
        print(f'Simulated day: {start_date}, Earnings so far: {coffee_bar.get_earnings()}', end='\r')

        # increment start date
        start_date = start_date + one_day

def simulate_modified(coffee_bar, ret_customers, probability_returner, start_date=datetime.date(2016, 1, 1), \
             end_date=datetime.date(2020, 12, 31)):
    """
    Simulates activities in the coffee bar.

    :param coffee_bar: Coffee_bar instance
    :param ret_customers: list of Returning Customers, entries are removed inside the method
    :param start_date: simulation start, used for iterating
    :param end_date: simulation end
    """

    ret_customers = ret_customers[:]
    timeslots = get_timeslots()
    one_day = datetime.timedelta(days=1)

    time_index = pd.read_csv('../Results/Part1/average_sales_food.csv', index_col='TIME_OF_DAY', sep=';').index.tolist()
    timeslots = [datetime.time.fromisoformat(time_str) for time_str in time_index]

    probability_returner['time_reformatted'] = [datetime.time.fromisoformat(time_str) for time_str in
                                  probability_returner['TIME_OF_DAY'].tolist()]


    # run the simulation in the given time
    while start_date <= end_date:

        for timeslot in timeslots:

            # create a datetime object for the timeslot
            time = datetime.datetime.combine(start_date, timeslot)

            prob = float(probability_returner['Yes'][probability_returner['time_reformatted']==timeslot])

            # random choice of the customer type
            # returning customers are chosen only if there are ones with enough budget
            # after that only one time customers can be chosen
            if ret_customers:
                customer_type = np.random.choice(['returning', 'one_time', 'tripadvisor'], 1, p=[prob,
                                                 (1-prob)*0.9, (1-prob)*0.1])[0]
            else:
                customer_type = np.random.choice(['one_time', 'tripadvisor'], 1, p=[0.9, 0.1])[0]

            if customer_type == 'returning':
            # choose a random returning customer and call the buy method
                ret_customer = np.random.choice(ret_customers, 1)[0]
                ret_customer.buy(time)

            # check if he has enough money and remove him from the list if not
                if not ret_customer.is_able_to_buy_most_expensive_food_drink():
                    ret_customers.remove(ret_customer)

            elif customer_type == 'one_time':
                one_time_customer = One_Time_Customer(coffee_bar)
                one_time_customer.buy(time)
            else:
                tripadvisor_customer = Tripadvisor_Customer(coffee_bar)
                tripadvisor_customer.buy(time)

            # print the current progress
        print(f'Simulated day: {start_date}, Earnings so far: {coffee_bar.get_earnings()}',
                  end='\r')

            # increment start date
        start_date = start_date + one_day


def get_timeslots():
    # get timeslots from the average_sales_drinks table, created with the exploratory script
    time_index = pd.read_csv('../Results/Part1/average_sales_food.csv', index_col='TIME_OF_DAY', sep=';').index.tolist()
    timeslots = [datetime.time.fromisoformat(time_str) for time_str in time_index]

    return timeslots

get_timeslots()