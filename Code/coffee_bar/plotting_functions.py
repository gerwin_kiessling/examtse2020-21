import datetime
import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import dates as mdates

from .coffee_bar_classes import Coffee_Bar, Returning_Customer, One_Time_Customer, \
                                  Tripadvisor_Customer, Hipster

def average_income_plot(dataframe_simulated, dataframe_real, prices_of_drinks, prices_of_food):

    """
    :param dataframe_simulated: dataframe with the simulated data
    :param dataframe_real: dataframe with the original data
    :param prices_of_drinks: dictionary with the drink prices
    :param prices_of_food: dictionary with the food prices
    :return: saved plot with average income at any given time
    """

    #initialize plots
    fig, ax = plt.subplots()

    #loop over dataframes to determine the average income at any given time
    for dataframe in [dataframe_real, dataframe_simulated]:
        dataframe['TIME'] = pd.to_datetime(dataframe['TIME'])
        # divide time in date and time of day
        dataframe['DATE'] = dataframe['TIME'].dt.date
        dataframe['TIME_OF_DAY'] = dataframe['TIME'].dt.time
        # set time as index
        dataframe = dataframe.set_index('TIME')

        #get income with or without tip. Only in the case of the simulated data, one knows the tip.
        if 'TIP' in dataframe.columns:

            dataframe['INCOME_TIP']=[prices_of_drinks[dataframe['DRINKS'][i]]+prices_of_food[dataframe['FOOD'][i]]+dataframe['TIP'][i]
                                  for i in range(len(dataframe))]


        dataframe['INCOME_NO_TIP']=[prices_of_drinks[dataframe['DRINKS'][i]]+prices_of_food[dataframe['FOOD'][i]]
                                  for i in range(len(dataframe))]

        #choose random day for the horizontal axis of the plot. Matplotlib requires a date.
        random_day = datetime.date(2010, 10, 10)

        #get the average income with and without tip for the simulated data and without tip for the original data
        if 'TIP' in dataframe.columns:

            for income in ['INCOME_TIP', 'INCOME_NO_TIP']:

                average_income=dataframe.groupby('TIME_OF_DAY')[income].mean().reset_index()
                combined_time = [datetime.datetime.combine(random_day, t) for t in average_income['TIME_OF_DAY']]

                ax.plot(combined_time, average_income[income], linewidth=0.5, label="Simulated Data with tip" if income=='INCOME_TIP' else
                                                                        "Simulated Data without tip")

        else:

            average_income=dataframe.groupby('TIME_OF_DAY')['INCOME_NO_TIP'].mean().reset_index()
            combined_time = [datetime.datetime.combine(random_day, t) for t in average_income['TIME_OF_DAY']]

            ax.plot(combined_time, average_income['INCOME_NO_TIP'], linewidth=0.5, label="Original Data (without tip)")

    timeformat = mdates.DateFormatter('%H:%M')
    plt.gcf().axes[0].xaxis.set_major_formatter(timeformat)
    plt.title('Average Income during the Day for Original and Simulated Data')
    ax.xaxis.set_label_text('Time of the Day')
    ax.yaxis.set_label_text('Average Income')
    ax.legend()
    plt.savefig('../Results/Part3/average_income')

def earnings_plot(dataframe_simulated, dataframe_real, prices_of_drinks, prices_of_food):

    """
    :param dataframe_simulated: dataframe with simulated data
    :param dataframe_real: dataframe with original data
    :param prices_of_drinks: dictionary with drink prices
    :param prices_of_food: dictionary with food prices
    :return: saved plot with total earnings per food and drink category
    """


    #initialize new dictionary
    earnings_from_source_real={}
    earnings_from_source_simulated={}

    for key in prices_of_drinks.keys():

        earnings_from_source_real[key]=sum(dataframe_real['DRINKS']==key)*prices_of_drinks[key]

    for key in prices_of_food.keys():

        earnings_from_source_real[key]=sum(dataframe_real['FOOD']==key)*prices_of_food[key]


    for key in prices_of_drinks.keys():

        earnings_from_source_simulated[key]=sum(dataframe_simulated['DRINKS']==key)*prices_of_drinks[key]

    for key in prices_of_food.keys():

        earnings_from_source_simulated[key]=sum(dataframe_simulated['FOOD']==key)*prices_of_food[key]

    #setting up the horizontal axis

    horizontal_axis = np.arange(0, len(earnings_from_source_real.keys())-1, 1)

    #adding subplots sequentially

    fig, ax = plt.subplots()

    ax.bar(horizontal_axis - 0.15, list(earnings_from_source_real.values())[:-1], width=0.2,  align='center',
           label="Original Dataset")
    ax.bar(horizontal_axis + 0.15, list(earnings_from_source_simulated.values())[:-1], width=0.2, align='center',
           label="Simulated Dataset")
    ax.set_xticks(horizontal_axis)
    ax.set_title('Total Earnings by Category')
    ax.set_xticklabels(list(earnings_from_source_simulated.keys())[:-1], rotation=20)
    ax.yaxis.set_label_text('Total Earnings')
    ax.legend()

    plt.savefig('../results/part3/earnings_by_source')



def customer_type_plot(dataframe, save_as):

    """
    :param dataframe: dataframe with the simulated data
    :param save_as: string used for saving
    :return: saved matplotlib plot
    """

    #specifying whether it is a returner or not
    dataframe['RETURNER'] = np.where(dataframe.CUSTOMER.isin(dataframe.CUSTOMER.value_counts().
                                                             index[dataframe.CUSTOMER.value_counts().gt(1)]), 'Yes', 'No')

    f = plt.figure(figsize=[15, 10])

    #iterate over food and drink

    for u, item in enumerate(['FOOD', 'DRINKS']):


        dataframe_returning=dataframe[dataframe['RETURNER']=='Yes']
        dataframe_non_returning=dataframe[dataframe['RETURNER']=='No']


        ax = f.add_subplot (2,1, u+1)


        x = np.arange(len(dataframe_returning[item].unique()))

        # two bars next to each other
        ax.bar(x-0.1, dataframe_returning[item].value_counts().sort_index().values/dataframe_returning.shape[0],
               width=0.1, label='Returners')
        ax.bar(x+0.1, dataframe_non_returning[item].value_counts().sort_index().values/dataframe_non_returning.shape[0],
               color='g', width=0.1, label='One-time customers')
        ax.set_title('Drink Sales')
        ax.xaxis.set_label_text(item)
        ax.set_xticks(x)
        ax.set_xticklabels(dataframe_returning[item].value_counts().sort_index().index)
        ax.yaxis.set_label_text('Share of Purchases within Customer Type Group')
        ax.legend()

    plt.savefig('../Results/Part4/{}'.format(save_as))

def probability_returner_plot(dataframe):

    """
    :param dataframe: the dataframe with the sales history
    :return: saved matplotlib plot
    """

    #choosing random day for plotting
    random_day = datetime.date(2010, 10, 10)

    combined_time = [datetime.datetime.combine(random_day, t) for t in dataframe.index]

    fig, ax= plt.subplots()

    ax.plot(combined_time, dataframe.values[:,1], linewidth=0.5, label="Probability of Having a Returning")
    plt.title('Probability of Having a Returning Customer')
    ax.xaxis.set_label_text('Time of the Day')
    ax.yaxis.set_label_text('Probability')
    timeformat = mdates.DateFormatter('%H:%M')
    plt.gcf().axes[0].xaxis.set_major_formatter(timeformat)
    plt.savefig('../Results/Part4/probability_returner')