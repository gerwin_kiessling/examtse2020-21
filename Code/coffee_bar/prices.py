# coffee shop prices for drinks as dictionary
drink_prices = {'milkshake': 5,
                'frappucino': 4,
                'water': 2,
                'soda': 3,
                'coffee': 3,
                'tea': 3}

# coffee shop prices for food as dictionary
food_prices = {'sandwich': 2,
               'cookie': 2,
               'pie': 3,
               'muffin': 3,
               'nothing': 0}