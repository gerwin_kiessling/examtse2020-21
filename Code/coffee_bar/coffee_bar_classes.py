import numpy as np
import pandas as pd
import uuid


class Coffee_Bar(object):
    """
    Represents a coffee bar. Customer can buy drinks and foods.
    The coffee bar keeps track of all sales and the earnings.
    """

    def __init__(self, drink_prices, food_prices):
        """
        :param drink_prices: dictionary with the drink's name as key and the price as value
        :param food_prices: dictionary with the food's name as key and the price as value
        """
        # sales history is a list, which contains other lists, where each list is representing a sale
        self.sales_history = list()
        self.drink_prices = drink_prices
        self.food_prices = food_prices
        self.earnings = 0

    def get_all_drink_prices(self):
        """
        :return: prices of the drinks as dictionary
        """
        return self.drink_prices

    def get_all_food_prices(self):
        """
        :return: prices of the food as dictionary
        """
        return self.food_prices

    def get_drink_price(self, drink):
        """
        :param drink: name of the drink
        :return: price
        """
        return self.drink_prices[drink]

    def get_food_price(self, food):
        """
        :param food: name of the food
        :return: price
        """
        return self.food_prices[food]

    def sell(self, time, customer_id, drink, food, tip=0):
        """
        The coffee bar sells a drink and/or food to a customer.
        The earnings of the sale are added to the overall earnings.
        The sale is added to the sale history.

        :param time: datetime.datetime object
        :param customer_id: id of the customer
        :param drink: name of the sold drink
        :param food: name of the sold food
        :param tip: tip, defaults to zero
        """
        self.earnings += self.get_drink_price(drink) + self.get_food_price(food) + tip
        self.sales_history.append([time.isoformat(' '), customer_id, drink, food, tip])

    def get_earnings(self):
        """
        :return: earnings
        """
        return self.earnings

    def get_sales_history(self):
        """
        :return: sales history
        """
        return self.sales_history


class Customer(object):
    """
    Base class of all customer types.
    The choice probabilities for drinks and food are the same for all customers.
    Therefore, this logic is placed inside the Customer class.
    """

    # class variables, since drink and food probabilities are the same for all objects
    drink_prob = pd.read_csv('../Results/Part1/average_sales_drinks.csv', index_col='TIME_OF_DAY', sep=';')
    food_prob = pd.read_csv('../Results/Part1/average_sales_food.csv', index_col='TIME_OF_DAY', sep=';')

    def __init__(self, coffee_bar, budget):
        """
        :param coffee_bar: Coffee_Bar object, the coffee bar, where the customer consumes
        :param budget: budget of the customer
        """
        # each customer has a unique id
        self.customer_id = uuid.uuid4()
        self.budget = budget
        self.coffee_bar = coffee_bar

    @classmethod
    def get_drink_food_choices(cls, timeslot):
        """
        The choice probabilities depend on the time of day.
        The time over the day is divided in multiple timeslots.
        This method creates a random choice with the corresponding probabilities.

        :param timeslot: time of the day in isoformat
        :return: name of the drink, name of the food
        """

        # the possible choices are the column names of the drink probabilities data frame
        # the corresponding probabilities are the values
        # since the values are a list in a list, we select the first entry
        # the choice method returns a list of length 1 (size = 1), we extract the value of the only element
        drink = np.random.choice(cls.drink_prob[cls.drink_prob.index == timeslot].columns.tolist(),
                                 size=1,
                                 p=cls.drink_prob[cls.drink_prob.index == timeslot].values.tolist()[0])[0]
        # same as for drink
        food = np.random.choice(cls.food_prob[cls.food_prob.index == timeslot].columns.tolist(),
                                size=1,
                                p=cls.food_prob[cls.food_prob.index == timeslot].values.tolist()[0])[0]

        return drink, food

    def buy(self, time, tip=0):
        """
        The customer buys products at the coffee shop.

        :param time: datetime.datetime object
        :param tip: tip, defaults to zero
        :return: names of the bought drink and food
        """

        # transform the timeslot to isoformat
        timeslot = time.time().isoformat()

        drink, food = self.get_drink_food_choices(timeslot)
        drink_price = self.coffee_bar.get_drink_price(drink)
        food_price = self.coffee_bar.get_food_price(food)

        # calculate the budget after the most recent purchases
        self.budget = self.budget - drink_price - food_price - tip

        # a buying customer comes as a pair with a selling coffee bar
        self.coffee_bar.sell(time, self.customer_id, drink, food, tip)

        return drink, food


class Returning_Customer(Customer):
    """
    A Returning customer can buy products as the same coffee bar multiple times.
    He keeps a record of his purchases.
    """
    def __init__(self, coffee_bar, budget=250):
        """
        :param coffee_bar: Coffee_Bar object, the coffee bar, where the customer consumes
        :param budget: budget of the customer
        """
        # call the init method of the parent class
        super().__init__(coffee_bar, budget)
        # create a purchase record
        self.purchase_history = list()

    def buy(self, time):
        """
        The Returning_Customer uses the buy method of his parent class.
        Additionally, all purchases are recorded in his purchase history.

        :param time: datetime.datetime object
        :return: names of the bought drink and food
        """
        drink, food = super().buy(time)
        self.purchase_history.append([time.isoformat(sep=' '), drink, food, self.budget])

        return drink, food

    def is_able_to_buy_most_expensive_food_drink(self):
        """
        Returns if the customer is able to buy the most expensive food and drink.
        :return: boolean
        """

        # get the prices of the most expensive food and drink
        most_exp_drink_price = max(self.coffee_bar.get_all_drink_prices().values())
        most_exp_food_price = max(self.coffee_bar.get_all_food_prices().values())

        return self.budget >= (most_exp_drink_price + most_exp_food_price)

    def get_purchase_history(self):
        """
        :return: record of purchases
        """
        return self.purchase_history


class Hipster(Returning_Customer):
    """
    Special Type of Returning Customer with a higher budget.
    """
    def __init__(self, coffee_bar):
        """
        :param coffee_bar: Coffee_Bar object, the coffee bar, where the customer consumes
        """
        # each hipster has a budget of 500
        super().__init__(coffee_bar, 500)


class Special_Hipster(Hipster):
    """
    Hipster which always buys the least chosen food or drink
    """
    def __init__(self, coffee_bar):
        """
        :param coffee_bar: Coffee_Bar object, the coffee bar, where the customer consumes
        """
        super().__init__(coffee_bar)

    def get_drink_food_choices(self, timeslot):
        # hipster always choose the least chosen food or drink
        if len(self.coffee_bar.get_sales_history()) > 100:
            df_sales_history = pd.DataFrame(self.coffee_bar.get_sales_history()[-100:],
                                            columns=['TIME', 'CUSTOMER', 'DRINKS', 'FOOD', 'TIP'])

            drink = df_sales_history['DRINKS'].value_counts().idxmin()
            food = df_sales_history['FOOD'].value_counts().idxmin()

        else:
            drink, food = super().get_drink_food_choices(timeslot)

        return drink, food


class One_Time_Customer(Customer):
    """
    One time customers visit the same coffee bar only once.
    """
    def __init__(self, coffee_bar, budget=100):
        """
        :param coffee_bar: Coffee_Bar object, the coffee bar, where the customer consumes
        :param budget: budget of the customer
        """
        super().__init__(coffee_bar, budget)


class Tripadvisor_Customer(One_Time_Customer):
    """
    Special type of one time customer, who found the coffee shop on the tripadvisor website
    In difference, to the one time customer he tips the coffee shop.
    """
    def __init__(self, coffee_bar):
        """
        :param coffee_bar: Coffee_Bar object, the coffee bar, where the customer consumes
        """
        super().__init__(coffee_bar)

    def buy(self, time):
        """
        The Returning_Customer uses the buy method of his parent class, but tips the coffee shop
        between 1 and 10 euro.

        :param time: datetime.datetime object
        :return: names of the bought drink and food
        """
        # create a random tip between 1 and 10
        tip = np.random.randint(1, 11)
        drink, food = super().buy(time, tip)

        return drink, food
