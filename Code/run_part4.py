import pandas as pd
import datetime
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import dates as mdates

from coffee_bar import simulation_functions as sim
from coffee_bar import plotting_functions as plotting
from coffee_bar.prices import drink_prices, food_prices

from coffee_bar.coffee_bar_classes import Coffee_Bar, Returning_Customer, Special_Hipster

#Buying histories

# simulate the coffee bar
coffee_bar, ret_customers = sim.prepare_simulation(drink_prices, food_prices)
sim.simulate(coffee_bar, ret_customers)

# Purchase history regular returning customers
for i in range(5):
    print(pd.DataFrame(ret_customers[i].get_purchase_history(),
                       columns=['TIME', 'DRINK', 'FOOD', 'BUDGET']))

# Purchase history hipsters
for i in range(1, 6):
    print(pd.DataFrame(ret_customers[-i].get_purchase_history(),
                       columns=['TIME', 'DRINK', 'FOOD', 'BUDGET']))


df = pd.read_csv("../Data/Coffeebar_2016-2020.csv", sep=';')

#Grouping by customer to check how many returners there are
df_grouped=df.groupby(['CUSTOMER']).size().reset_index(name='counts')

number_returners=sum(df_grouped['counts']>1)
print('There are {} returning customers.'.format(number_returners))

###preparing the dataframe to group by the time of the day
# change datatype of column time to datetime
df['TIME'] = pd.to_datetime(df['TIME'])
# divide time in date and time of day
df['DATE'] = df['TIME'].dt.date
df['TIME_OF_DAY'] = df['TIME'].dt.time
# set time as index
df = df.set_index('TIME')

df = df.fillna('nothing')

#check whether the customer is a returner
df['RETURNER'] = np.where(df.CUSTOMER.isin(df.CUSTOMER.value_counts().
                                           index[df.CUSTOMER.value_counts().gt(1)]), 'Yes', 'No')

group_time = df.groupby(['TIME_OF_DAY'])

df_type = group_time['RETURNER'].value_counts(normalize=True).to_frame(name='SHARE')

df_type = df_type.reset_index()
# reformat the dataframe using the pivot function
df_type = df_type.pivot(index='TIME_OF_DAY', columns='RETURNER', values='SHARE')
df_type.to_csv('../Results/Part4/probabilities_customer_type.csv', sep=';')

probs_customer=pd.read_csv("../Results/Part4/probabilities_customer_type.csv", sep=';')
#plotting the probabilites
plotting.probability_returner_plot(dataframe=df_type)

###comparing the buying decisions of one-time customers and returners
plotting.customer_type_plot(df, 'compare_buying_baseline')

###comparing the buying decisions of one-time customers and returners when there are fewer returners
my_coffee_bar_few_returners, ret_customers_few_returners = sim.prepare_simulation(drink_prices,
                                                                                  food_prices, no_ret_customers = 34, no_hipster = 16)
sim.simulate_modified(my_coffee_bar_few_returners, ret_customers_few_returners, probs_customer)
df_sales_history_few_returners = pd.DataFrame(my_coffee_bar_few_returners.get_sales_history(),
                                columns=['TIME', 'CUSTOMER', 'DRINKS', 'FOOD', 'TIP'])
plotting.customer_type_plot(df_sales_history_few_returners, 'compare_buying_few_returners')

### implementing price changes
my_coffee_bar_high_prices, ret_customers_high_prices = sim.prepare_simulation(drink_prices, food_prices)
sim.simulate_modified(my_coffee_bar_high_prices, ret_customers_high_prices, probs_customer, start_date=datetime.date(2016, 1, 1), end_date=datetime.date(2017, 12, 31))

# raise drink prices by 20 percent
for drink in coffee_bar.drink_prices:
    coffee_bar.drink_prices[drink] = round(coffee_bar.drink_prices[drink] * 1.2, 2)

# raise food prices by 20 percent
for food in coffee_bar.food_prices:
    coffee_bar.food_prices[food] = round(coffee_bar.food_prices[food] * 1.2, 2)

# create updated list of returning customers with enough budget to buy most expensive products
wealthy_ret_customers = [customer for customer in ret_customers_high_prices if customer.is_able_to_buy_most_expensive_food_drink()]

# simulate the last years with updated prices
sim.simulate_modified(my_coffee_bar_high_prices, wealthy_ret_customers, probs_customer, start_date=datetime.date(2018, 1, 1), end_date=datetime.date(2020, 12, 31))
df_sales_history_high_prices = pd.DataFrame(my_coffee_bar_high_prices.get_sales_history(),
                                columns=['TIME', 'CUSTOMER', 'DRINKS', 'FOOD', 'TIP'])
plotting.customer_type_plot(df_sales_history_high_prices, 'compare_buying_price_change')

###comparing the buying decisions of one-time customers and returners when returners (excluding hipsters) have a low budget
my_coffee_bar_low_budget, ret_customers_low_budget = sim.prepare_simulation(drink_prices, food_prices, budget_returner=40)
sim.simulate_modified(my_coffee_bar_low_budget, ret_customers_low_budget, probs_customer)
df_sales_history_low_budget = pd.DataFrame(my_coffee_bar_low_budget.get_sales_history(),
                                columns=['TIME', 'CUSTOMER', 'DRINKS', 'FOOD', 'TIP'])
plotting.customer_type_plot(df_sales_history_low_budget, 'compare_buying_low_budget')

### Question: What happens if hipster buy the least bought drink and food every time?
### A new class of hipsters was created in coffee_bar_classes.py

coffee_bar = Coffee_Bar(drink_prices, food_prices)

reg_ret_customers = list()
for i in range(0, 667):
    reg_ret_customers.append(Returning_Customer(coffee_bar))

hipster = list()
for i in range(0, 333):
    hipster.append(Special_Hipster(coffee_bar))

sim.simulate(coffee_bar, reg_ret_customers + hipster)

df_sales_history = pd.DataFrame(coffee_bar.get_sales_history(),
                                columns=['TIME', 'CUSTOMER', 'DRINKS', 'FOOD', 'TIP'])

# bar plot for drink sales
fig, ax = plt.subplots()
ax.bar(df_sales_history['DRINKS'].value_counts().index, df_sales_history['DRINKS'].value_counts().values)

ax.set_title('Drink Sales')
ax.xaxis.set_label_text('Drinks')
ax.yaxis.set_label_text('Sales')

plt.savefig('../Results/Part4/special_hipsters_bar_plot')
