import matplotlib.pyplot as plt
import pandas as pd

####################
# DATA PREPARATION #
####################

# load data file
df = pd.read_csv("../Data/Coffeebar_2016-2020.csv", sep=';')

# change datatype of column time to datetime
df['TIME'] = pd.to_datetime(df['TIME'])
# divide time in date and time of day
df['DATE'] = df['TIME'].dt.date
df['TIME_OF_DAY'] = df['TIME'].dt.time
# set time as index
df = df.set_index('TIME')

# replace NAs by 'nothing' because they would be omitted by the value_counts method
df = df.fillna('nothing')

##########################
# FIRST DATA EXPLORATION #
##########################

# calculate and print simple statistics
print(f"The bar sells {len(set(df['FOOD']))} types of food: {set(df['FOOD'])}")
print(f"The bar sells {len(set(df['DRINKS']))} types of food: {set(df['DRINKS'])}")
print(f"The bar has {len(set(df['CUSTOMER']))} customers in total")

#########
# PLOTS #
#########

# bar plot for drink sales
fig, ax = plt.subplots()
ax.bar(df['DRINKS'].value_counts().index, df['DRINKS'].value_counts().values)

ax.set_title('Drink Sales')
ax.xaxis.set_label_text('Drinks')
ax.yaxis.set_label_text('Sales')

plt.savefig('../Results/Part1/drink_bar_plot')

# bar plot for food sales
fig, ax = plt.subplots()
ax.bar(df['FOOD'].value_counts().index, df['FOOD'].value_counts().values)

ax.set_title('Food Sales')
ax.xaxis.set_label_text('Foods')
ax.yaxis.set_label_text('Sales')

plt.savefig('../Results/Part1/food_bar_plot')

# plot trend in drink sales
fig, ax = plt.subplots()

# add a line for every drink showing the monthly sales
for drink in set(df['DRINKS']):
    drink_count = df[df['DRINKS'] == drink].groupby(pd.Grouper(freq='M')).count()
    ax.plot(drink_count.index, drink_count['DRINKS'], label=drink)

ax.legend()

ax.set_title('Monthly Drink Sales')
ax.xaxis.set_label_text('Time')
ax.yaxis.set_label_text('Sales')

plt.savefig('../Results/Part1/drink_sales_trend')

# plot trend in food sales
fig, ax = plt.subplots()

# add a line for each food showing the monthly sales
for food in set(df['FOOD']):
    food_count = df[df['FOOD'] == food].groupby(pd.Grouper(freq='M')).count()
    ax.plot(food_count.index, food_count['DRINKS'], label=food)

ax.legend()

ax.set_title('Monthly Food Sales')
ax.xaxis.set_label_text('Time')
ax.yaxis.set_label_text('Food')

plt.savefig('../Results/Part1/food_sales_trend')

################################
#  AVERAGE SALES PER TIME SLOT #
################################

# group the dataframe by time of day
group_time = df.groupby(['TIME_OF_DAY'])

# calculate the share of the drinks sold per time slot and transfer the result to a data frame
df_drinks = group_time['DRINKS'].value_counts(normalize=True).to_frame(name='SHARE')

# calculate the share of the food sold per time slot and transfer the result to a data frame
df_food = group_time['FOOD'].value_counts(normalize=True).to_frame(name='SHARE')

# to use time of index as index in the pivot function
df_drinks = df_drinks.reset_index()
# reformat the dataframe using the pivot function
df_drinks = df_drinks.pivot(index='TIME_OF_DAY', columns='DRINKS', values='SHARE')
# to correct that by the pivot transformation NAs are added for drinks with share equal zero
df_drinks = df_drinks.fillna(0)
# save dataframe as csv
df_drinks.to_csv('../Results/Part1/average_sales_drinks.csv', sep=';')

# to use time of index as index in the pivot function
df_food = df_food.reset_index()
# reformat the dataframe using the pivot function
df_food = df_food.pivot(index='TIME_OF_DAY', columns='FOOD', values='SHARE')
# to correct that by the pivot transformation NAs are added for drinks with share equal zero
df_food = df_food.fillna(0)
# save dataframe as csv
df_food.to_csv('../Results/Part1/average_sales_food.csv', sep=';')
