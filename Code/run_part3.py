import datetime
import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import dates as mdates

from coffee_bar.coffee_bar_classes import Coffee_Bar, Returning_Customer, One_Time_Customer, \
                                  Tripadvisor_Customer, Hipster

from coffee_bar import simulation_functions as sim
from coffee_bar import plotting_functions as plotting
from coffee_bar.prices import drink_prices, food_prices

my_coffee_bar, ret_customers = sim.prepare_simulation(drink_prices, food_prices)
sim.simulate(my_coffee_bar, ret_customers)

# save sales history as csv file, as in the given sales data
df_sales_history = pd.DataFrame(my_coffee_bar.get_sales_history(),
                                columns=['TIME', 'CUSTOMER', 'DRINKS', 'FOOD', 'TIP'])
df_sales_history.to_csv('../Results/Part3/simulation_sales_history.csv', sep=';')

df_empirical_data = pd.read_csv("../Data/Coffeebar_2016-2020.csv", sep=';')
df_empirical_data = df_empirical_data.fillna('nothing')

#call the average income plot function

plotting.average_income_plot(df_sales_history, df_empirical_data, drink_prices, food_prices)

#call the earnings plot function

plotting.earnings_plot(df_sales_history, df_empirical_data, drink_prices, food_prices)